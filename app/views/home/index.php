<!DOCTYPE html>
    <head>  
        <title>Bulletin Board</title>
        <link rel="stylesheet" type="text/css" href="<?= BASEURL; ?>/css/style.css">
    </head>
    
    <body>
        <div class="bulletin">
            <div class="notification">
                <?php if (isset($data['error_msg'])) :
                    if ($data['error_msg'] != '') :?>
                        <p>
                            <b>Please correct the following errors:</b><br />
                            <?php echo $data['error_msg'];?> 
                        </p>
                    <?php endif; ?>
                <?php endif;?>
            </div>
            <div class="input-panel">
                <form method="POST" action="<?= BASEURL . '/home/addmessage'; ?>/">
                    <h4>Title</h4>
                    <input type="text" name="title"><br>  <!-- value="<?php echo $data['title'];?>" -->
                    <h4>Body</h4>
                    <textarea rows="4" cols="50" name="body"></textarea> <!-- <?php echo $data['body'];?> -->
                    <br>
                    <input type="submit" name="submit" value="Submit">
                </form>
            </div>

            <br>

            <div class="output-panel">
                <?php foreach ($data['message'] as $msg) :?>
                    <div class="message">
                    <h4><?= $msg['title']?></h4>
                    <p><?= $msg['body']?></p>
                    <h6><?= date('d/m/Y H:i', strtotime($msg['timestamp']))?></h6>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </body>
</html>