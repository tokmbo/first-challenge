<?php

class Home extends Controller 
{
    public function model()
    {
        require_once '../app/models/Message_model.php';
        return new Message_model;
    }

    public function index($msg = null)
    {
        $data['message']    = $this->model()->getAllMessages();
        $data['error_msg']  = $msg;
        $this->view('home/index', $data);
    }

    public function addMessage()
    {
        if (!isset($_POST['submit'])) {
            $this->index();
        }

        $title = $_POST['title'];
        $body = $_POST['body'];

        $errors = $this->validate($title, $body);
        if (empty($errors)) {
            $data['message']    = $this->model()->addMessage($title,$body);
            header('location: /challenge30/public');
        } else {
            $this->index($errors);
        }
    }

    public function validate($title, $body) {
        $errors  = array();
        $values  = array();
        $errmsg  = '';

        if (empty($title)) {
        $errors[] = 'Title must be filled.';
        }

        if (strlen($title) < 10 ||
            strlen($title) > 30) {
            $errors[] = "Title's length must be between 10 and 30 characters.";
        }

        if (empty($body)) {
            $errors[] = 'Body must be filled.';
        }

        if (strlen($body) < 10 ||
            strlen($body) > 200) {
            $errors[] = "Body's length must be between 10 and 200 characters.";
        }

        if (sizeof($errors) == 0) {
            $values = array();
            return $errmsg;
        } else {
            foreach($errors as $error) {
                $errmsg .= $error . '<br />';
            }
            return $errmsg;
        }
    }

}