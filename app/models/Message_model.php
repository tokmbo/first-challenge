<?php

class Message_model
{

    private $table = 'messages';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllMessages()
    {
        $result = $this->db->getAll($this->table, 'DESC', 'timestamp');
        return $result;
    }

    public function addMessage($title, $body)
    {
        $this->db->add($this->table, $title, $body);
    }

}