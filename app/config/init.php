<?php

define('BASEURL', strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))
    =='https://'?'https://':'http://' 
    . $_SERVER['HTTP_HOST'] 
    . '/challenge30/public');


require_once '../app/core/App.php';
require_once '../app/core/Controller.php';
require_once '../app/core/Database.php';

require_once '../app/config/database.php';