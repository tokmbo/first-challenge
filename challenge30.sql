-- phpMyAdmin SQL Dump s
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 06, 2019 at 05:18 AM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `challenge30`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `body` varchar(200) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `title`, `body`, `timestamp`) VALUES
(1, 'Nama saya Budi', 'Budi adalah nama saya.', '2019-11-04 05:38:11'),
(2, 'Nama saya Agung', 'Agung adalah nama saya.', '2019-11-04 05:38:11'),
(3, 'Nama saya Jokowi', 'Jokowi adalah nama saya.', '2019-11-04 06:06:34'),
(4, 'Nama saya Jokowow', 'Jokowow adalah nama saya.', '2019-11-04 06:10:16'),
(5, 'Nama saya Jokowowi', 'Jokowowi adalah nama saya.', '2019-11-04 06:11:38'),
(6, 'Manchester United', 'Manchester united is biggest club in the world.', '2019-11-04 06:44:35'),
(7, 'Testing pagi ini', 'ahdkhad dajdakds daksjhaks sakfjhaskf', '2019-11-04 22:30:02'),
(11, 'Nyoba test lagi', 'dkadadhadjad adkhaskdjhad dkHADKJAHKDjaD', '2019-11-04 22:47:33'),
(12, 'Tes ketiga hari ini', 'lahslasd alsdlasjd aslasdljalsjd als jdla sjdlas lad', '2019-11-05 00:43:38'),
(13, 'Tes keempat hari ini', 'DKJFHSDJ SF skjfhsdjkhf dskfhsd', '2019-11-05 00:44:14'),
(14, 'Nama saya Jokowi 5', 'jjjjjjjjjjjjjjjjjjjjjjjjj', '2019-11-05 07:10:58'),
(16, 'Test malam hari', 'daduiyad daidya  ada ida da iauday', '2019-11-05 13:43:44'),
(17, 'Testing pagi iniaaa', 'ahkahkjaha  kajha ak ah aj ajkha', '2019-11-05 14:37:43'),
(18, 'Tes ketiga hari ini ff', 'fefsfds dfdsfsd dsfdsdsffs', '2019-11-05 15:27:24'),
(19, 'Tes ketiga hari ini ffg', 'sgsdgsd sgsdgs sdgsdgsd', '2019-11-05 19:48:54'),
(20, 'Test via validate', 'dahjadjah ahdjahda jahdjajda aj hdajhd', '2019-11-05 19:55:09'),
(21, 'Test add message', 'hdahdajda dhajdadhja ajhdajhda', '2019-11-05 19:59:04'),
(22, 'Tes add messages after ', 'adadddddddddddddddddddddddddddddd', '2019-11-05 20:48:08'),
(23, 'Tesldjks sdlkslds sdsd', 'dahkjdhajd adahkjda adahdk', '2019-11-05 21:13:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
